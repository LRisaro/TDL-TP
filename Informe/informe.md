![alt text](/home/lucas/Documentos/Facultad/logo-FIUBA.bin)
#  **Teoria del Lenguaje**
## **Integrantes**:
*  Risaro, Lucas Nahuel
*  Castro Pippo, Juan Manuel
*  Palmeira, Agustin

Copyright (c) 2017 Copyright Holder All Rights Reserved.

<div style="page-break-after: always;"></div>
# **Respuestas a Ejercicios Teoricos**

## 1. **Ejercicio 1:**
Archivos en repositorio (por ahora)

## 2. **Ejercicio 2:**

  a) U,Q.
  b) Q.
  c) No hay referencias externas.


## 1. **Ejercicio 7: Currying**
Currying es una tecnica mediante la cual se transforma una funcion que recibe multiples argumentos en una secuencia de funciones que utilizan un único argumento.
