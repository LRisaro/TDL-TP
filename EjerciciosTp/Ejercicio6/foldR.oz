local MiFoldR  in
   fun {MiFoldR L F N}
      local R in
	 R = {List.reverse L}
	 case R of H|T then
	    {MiFoldR T F {F N H}}
	 else N
	 end
      end
   end

   
   {Browse {MiFoldR [1 2 3 4] fun {$ X Y} X-Y end 8}}
end