local MiFoldL in
   fun {MiFoldL List F N}
      case List of H|T then
	 {MiFoldL T F {F N H}}
      else N
      end
   end

   {Browse {MiFoldL [1 2 3 4] fun {$ X Y} X-Y end 2}}
end
