local MiFilter in
   fun {MiFilter List F}
      case List of H|T then
	 if {F H} then H|{MiFilter T F}
	 else
	    {MiFilter T F}
	 end
      else
	 nil
      end
   end

   {Browse {MiFilter [1 2 3 4 5] fun {$ A} A=<3 end}}
end
