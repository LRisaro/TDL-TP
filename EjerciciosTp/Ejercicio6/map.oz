local MiMap in
   fun {MiMap List F}
      case List of H|T then {F H}|{MiMap T F}
      else nil
      end
   end

   {Browse {MiMap [1 2 3] fun {$ I} I*I end}}
end
