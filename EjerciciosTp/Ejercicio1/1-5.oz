local Member X Y in
   fun {Member X Y}
      local H T in
	 case X of H|T then
	    if Y==H then Y==H else {Member T Y} end
	 else
	    Y==X
	 end	 
      end
   end
   
   X=[1 2 'l' 4 5]
   Y='t'
   {Browse {Member X Y}}
end
