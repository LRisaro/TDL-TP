local Append L X in
   fun {Append L X}
      case L#X of nil#_ then X
      [] _#nil then L
      [] (LH|LT)#(XH|XT) then
	 LH|XH|{Append LT XT}
      end
   end


   L=[1 3 5 7 8 9]
   X=[2 4 6]
   {Browse {Append L X}}
end
