local Drop in
   
   fun {Drop L N}
      local LH LT in
	 if {List.length L} >= N then
	    LH|LT=L case N of 1 then LT else {Drop LT N-1} end
      else
	 nil
	 end
      end
   end

   {Browse {Drop [1 2 35 6 8 60] 2}}
end
