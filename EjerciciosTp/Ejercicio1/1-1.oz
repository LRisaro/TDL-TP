local Length L in
   fun {Length L}
      case L of H|T then 1+{Length T}
      else
	 0
      end
   end

   {Browse {Length [1 2 3 4 20]}}
end



   