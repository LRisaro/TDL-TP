local Take in
   
   fun {Take L N}
      local LT LH in
	 if {List.length L} >= N then
	    LH|LT=L case N of 1 then LH else LH|{Take LT N-1} end
	 else L
	 end
      end
   end

   {Browse {Take [1 2 3 4 5 6] 7}}
end